﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {
	public Transform generationPoint;
	public GameObject[] backgrounds;
	public float distance;
	
	public static string bgState = null;
	int pos = 0;
	int selector;
	
	// Update is called once per frame
	void Update () {
		checkState ();
		ProcessingBG ();

	}
	void checkState(){
		int score = Character.scoore;
		
		if (score < 25) {
			bgState = "tower";
		} else if (score >= 25 && score < 75) {
			bgState = "sky";
		}else if(score > 75){
			bgState = "space";
		}
	}

	void bgConfig(){
		switch (bgState) {
		
		case "tower" : 
			selector = 0;
			break;
		case "sky" : 
			if(pos == 0){
				selector = 1;
				pos = 1;
			}else if(pos == 1){
				selector = 2;
				pos = 2;
			}else if(pos == 2){
				selector = 3;
			}

			break;
		case "space" : 
			if(pos == 2){
				selector = 4;
				pos = 3;
			}else{
				selector = Random.Range(5,7);
			}
			break;
		}
	}

	void ProcessingBG(){

		if (transform.position.y < generationPoint.position.y){
			bgConfig ();
			transform.position = new Vector3 (transform.position.x, transform.position.y  + distance, 10.95f);
			Instantiate (backgrounds[selector], transform.position, Quaternion.identity);

		}
	}
}
