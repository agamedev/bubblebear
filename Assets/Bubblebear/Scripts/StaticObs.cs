﻿using UnityEngine;
using System.Collections;

public class StaticObs : Obstacle {
	public Transform generationPoint;
	public float distance;
	public GameObject cam;
	public GroundPooler[] obsPools;

	int platformSelector;
	int i=9;
	void Start(){


	}

	// Update is called once per frame
	void Update () {
		ProcessingPlatform();
	}

	IEnumerator spawnStar(int pos){
		int time = Random.Range (2,4);
		yield return new WaitForSeconds (time);

	}



	
	void SpawnPlatform(int x){

		float axis = 0f;
		switch (x) {
			
		case 0: axis = -1.8f;
			break;
		case 1: axis = 1.7f;
			break;
		case 2: axis = 0.07f;
			break;
		}
		int a = Random.Range (0,2);
		 
		GameObject newGround = obsPools [a].getObject ();
		newGround.transform.position = new Vector3(axis,transform.position.y,0);
		newGround.transform.rotation = transform.rotation;
		newGround.SetActive(true);
		i = x;
		
	}

	void ProcessingPlatform(){
		if (transform.position.y < generationPoint.position.y && Background.bgState != "trans1"){
			
			transform.position = new Vector3 (transform.position.x, transform.position.y  + distance, 10f);
		
			do{
				platformSelector = Random.Range(0,3);
			}while(platformSelector == i);	
			
			SpawnPlatform(platformSelector);
			
		}
	}
	
	
}