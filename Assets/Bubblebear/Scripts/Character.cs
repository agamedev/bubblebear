﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Character : MonoBehaviour {
	public GameObject cam;
	public GameObject lose;
	public Text scoreDisplay;

	float speed = 0.4f;
	public static int scoore;

	bool isMoving = false;
	public bool dead = false;


	// Use this for initialization
	void Start () {	
		scoore = 0;
		transform.position = new Vector3(-0.1f, cam.transform.position.y - 2.495f, 0f);
	}

	void Score(){
		scoore += 1;
		GameObject.Find ("Healthbar").GetComponentInChildren<Text> ().text = scoore.ToString ();
	}

	// Update is called once per frame
	void Update () {
		checkArmor ();
#if UNITY_ANDROID
		if (Input.touchCount > 0) {
				Touch t = Input.GetTouch(0);
				if(t.phase == TouchPhase.Began){
				if(!isMoving ){
					Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
					Vector2 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
					Vector3 endP = new Vector3(Mathf.Clamp(curPosition.x,-2.5f,2.5f),Mathf.Clamp(curPosition.y,cam.transform.position.y-3.9f,cam.transform.position.y +0.9f),0);
					if(Powerup.isHaste || Powerup.isSlow){
						speed = 0.2f;
					}else{
						speed = 0.4f;
					}
					StartCoroutine(MovePlayer(this.transform,transform.position,endP, speed));
				}
				}
				
				if(t.phase == TouchPhase.Moved){
				}

				if(t.phase == TouchPhase.Ended){
				}
		}
#endif
#if UNITY_EDITOR
		if (Input.GetMouseButtonDown (0)) {
			if(!isMoving ){
				Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
				Vector2 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
				Vector3 endP = new Vector3(Mathf.Clamp(curPosition.x,-2.5f,2.5f),Mathf.Clamp(curPosition.y,cam.transform.position.y-3.9f,cam.transform.position.y +0.9f),0);
					if(Powerup.isHaste || Powerup.isSlow){
						speed = 0.2f;
					}else{
						speed = 0.4f;
					}
						StartCoroutine(MovePlayer(this.transform,transform.position,endP, speed));

			}
		}
#endif

	}

	void checkArmor(){
		if (Powerup.isArmored) {
			transform.GetChild(0).gameObject.SetActive(true);
		} else {
			transform.GetChild(0).gameObject.SetActive(false);
		}
	}

	IEnumerator MovePlayer(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time){
		var i= 0.0f;

		var rate= 1.0f/time;
		isMoving = true;
		while (i < 1.0f) {
			i += Time.deltaTime * rate;
			thisTransform.position = Vector3.Lerp(startPos, endPos, i);
			yield return null; 
		}
		isMoving = false;
	}

	public void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Obs") {
			if(!Powerup.isInvicible){
				if(Powerup.isArmored){
					Powerup.isArmored = false;
				}else{
					Time.timeScale = 0f;
					gameObject.SetActive(false);
					scoreDisplay.text = scoore.ToString();
					lose.SetActive(true);
				}

			}
			
		}else if(col.tag == "Powerup"){
			cam.GetComponentInChildren<Inventory>().getItem(col.gameObject);
			Destroy(col.gameObject);
		}else if(col.tag == "Star"){
			Score();
			Destroy(col.gameObject);
		}
	}

}
