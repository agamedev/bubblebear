﻿using UnityEngine;
using System.Collections;

public class Powerup : MonoBehaviour {

	public static bool isInvicible = false;
	public static bool isSlow = false;
	public static bool isHaste = false;
	public static bool isArmored = false;

	public void getPowerup(string name){
		StartCoroutine (name);
	}

	IEnumerator TimeSlow(){
		isSlow = true;
		Time.timeScale = 0.5f;
		yield return new WaitForSeconds(5);
		Time.timeScale = 1f;
		isSlow = false;

	}

	IEnumerator CleanZone(){
		GameObject[] obs = GameObject.FindGameObjectsWithTag ("Obs");
		for(int i=0; i< obs.Length; i++){
			if(obs[i].activeSelf){
				obs[i].SetActive(false);
			}
		}

		yield return null;
	}

	IEnumerator Armour(){
		isArmored = true;
		yield return null;
	}

	IEnumerator HasteFly(){
		isHaste = true;
		yield return new WaitForSeconds(5);
		isHaste = false;
	}

	IEnumerator Invincible(){
		isInvicible = true;
		yield return new WaitForSeconds(5);
		isInvicible = false;
	}
}
