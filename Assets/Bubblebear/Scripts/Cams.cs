﻿using UnityEngine;
using System.Collections;

public class Cams : MonoBehaviour {
	public float speed;
	// Use this for initialization
	void Start () {
		Time.timeScale = 1f;
		InvokeRepeating ("camSpeed", 0, 8);
	}
	void camSpeed(){
		speed += 0.2f;
	}
	// Update is called once per frame
	void Update () {

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> (). velocity.x, speed);
	}
}
