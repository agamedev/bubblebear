﻿using UnityEngine;
using System.Collections;

public class DynamicObs : Obstacle {
	public GameObject[] obsV, obsH;
	public GameObject sign;
	public float speed;

	Transform[] parent = new Transform[2];
	bool isV = false;

	// Use this for initialization
	void Start () {
		parent [0] = gameObject.transform.GetChild (0).transform;
		parent [1] = gameObject.transform.GetChild (1).transform;
		InvokeRepeating ("pickObs",20.0f,10.0f);
	}

	protected Vector3 generatePosV(){
		float x = Random.Range (-2.0f,3.0f);
		return new Vector3(x, parent[0].position.y, 10);
	}
	
	protected Vector3 generatePosH(){
		float y = Random.Range (-4.0f,4.0f);
		return new Vector3( parent[1].position.x,parent[1].position.y + y, 10);
	}

	protected void pickObs(){
		int a = Random.Range (0,4);
		Vector3 signPos, rndm;
		if (a == 0 || a == 2) {
			rndm = generatePosV ();
			signPos = new Vector3 (rndm.x,rndm.y - 4.5f, rndm.z);
			isV = true;
		} else {
			rndm = generatePosH ();
			signPos = new Vector3 (Mathf.Clamp(rndm.x,-2.7f,-2.8f), rndm.y, rndm.z); 
			isV = false;
		}

		StartCoroutine (SpawnObs(sign, rndm, signPos, isV));

	}

	protected IEnumerator SpawnObs(Object obj,Vector3 pos, Vector3 signPos, bool isV){
		GameObject myNewInstance;
		Vector3 newPos;
		GameObject sign = (GameObject)Instantiate (obj, signPos, Quaternion.identity);

		if (isV) {
			sign.transform.parent = parent[0];
		} else {
			sign.transform.parent = parent[1];
		}

		for (int j=0; j<3; j++) {
			sign.SetActive (false);
			yield return new WaitForSeconds (1);
			sign.SetActive (true);
			yield return new WaitForSeconds (0.2f);
		}
		Destroy (sign);
		int a = 0;
		if (isV) {
			switch(Background.bgState){
				case "tower": a = Random.Range(0,2); break;
				case "sky": a = 3; break;
				case "space": a = 4; break;
			}

			myNewInstance = (GameObject) Instantiate(obsV[a],sign.transform.position, obsV[a].transform.rotation);
			newPos = new Vector3 (pos.x,sign.transform.position.y - 16, 10);
		} else {
			switch(Background.bgState){
			case "tower": a = 0; break;
			case "sky": a = 1; break;
			case "space": a = 0; break;
			}

			myNewInstance = (GameObject) Instantiate(obsH[a],sign.transform.position, obsH[a].transform.rotation);
			newPos = new Vector3(7.5f,sign.transform.position.y +1f, 10);
		}

		StartCoroutine (MoveObject(myNewInstance.transform,myNewInstance.transform.position,newPos, speed));
		StartCoroutine (delete(myNewInstance));
	}


}
