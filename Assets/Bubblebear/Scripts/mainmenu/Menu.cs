﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	private Animator _animator;
	// Use this for initialization
	public void Awake () {
		_animator = GetComponent<Animator> ();

		var rect = GetComponent<RectTransform> ();
		rect.offsetMax = rect.offsetMin = new Vector2 (0, 0);
	}

	public bool IsOpen{
		get { return _animator.GetBool("IsOpen");}
		set { _animator.SetBool("IsOpen", value);}
	}


}
