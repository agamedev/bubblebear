﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {
	public Menu CurrentMenu;

	public static bool music = true;
	public static bool sound;

	// Use this for initialization
	void Start () {
		ShowMenu (CurrentMenu);
		Time.timeScale = 1;

		//MusicController ();
	}
	
	public void ShowMenu(Menu menu){
		if (CurrentMenu != null)
			CurrentMenu.IsOpen = false;

		SoundBase.instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.instance.tapButton);
		CurrentMenu = menu;
		CurrentMenu.IsOpen = true;
	}

	public void Play(){
		
		SoundBase.instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.instance.tapButton);
		Application.LoadLevel ("bubblebearGamePlay");
	}

	public void SoundOff(GameObject Off) {
		//Debug.Log (!Off.activeSelf);
		Off.SetActive(true);
		GameObject.Find ("SoundOn").SetActive (false);
		SoundBase.instance.GetComponent<AudioSource> ().volume = 0;

	}
	public void SoundOn(GameObject On) {
		//Debug.Log (!Off.activeSelf);
		On.SetActive(true);
		GameObject.Find ("SoundOff").SetActive (false);
		SoundBase.instance.GetComponent<AudioSource> ().volume = 1;
	}

	public void MusicOff(GameObject Off) {
		//Debug.Log (!Off.activeSelf);
		music = false;
		if (music == false) {
			Off.SetActive (true);
			GameObject.Find ("MusicOn").SetActive (false);
			MusicBase.instance.GetComponent<AudioSource> ().volume = 0;
		}

	}
	public void MuscinOn(GameObject On) {
		//Debug.Log (!Off.activeSelf);
		music = true;
		if (music == true) {
			On.SetActive (true);
			GameObject.Find ("MusicOff").SetActive (false);
			MusicBase.instance.GetComponent<AudioSource> ().volume = 1;
		}
	}
}
