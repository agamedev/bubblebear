﻿using UnityEngine;
using System.Collections;

public class MusicBase : MonoBehaviour {
	public AudioSource musicSource;
	public static MusicBase instance = null;

	// Use this for initialization
	public void Start () {
		if (instance == null)
			instance = null;
		else if (instance != null)
			Destroy (gameObject);

			//DontDestroyOnLoad (gameObject);
		instance = this;
	}
}
