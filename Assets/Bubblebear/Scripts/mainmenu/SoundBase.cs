﻿using UnityEngine;
using System.Collections;

public class SoundBase : MonoBehaviour {

	public AudioSource efxSource;
	public AudioClip tapButton;

	public static SoundBase instance = null;


	// Use this for initialization
	void Awake () {
		if (instance == null)
			instance = null;
		else if (instance != null)
			Destroy (gameObject);

		//DontDestroyOnLoad (gameObject);
		instance = this;
	}
}
