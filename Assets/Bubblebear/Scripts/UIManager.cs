﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {
	public GameObject pause;
	
	public void RestartGame(){
		Application.LoadLevel (Application.loadedLevel);
	}
	
	public void ExitGame(){
		Application.Quit ();
	}

	public void PauseGame(){
		pause.SetActive (true);
		Time.timeScale = 0;

	}
	public void ResumeGame(){
		Time.timeScale = 1;
		GameObject.Find ("PausePanel").SetActive (false);
	}

}
