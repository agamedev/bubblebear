﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GroundPooler : MonoBehaviour {

	public GameObject pooledObject;
	public int amount;
	List<GameObject> ObjectList;
	// Use this for initialization
	void Start () {
		ObjectList = new List<GameObject> ();
	
		for (int i = 0; i < amount; i++) {
			GameObject obj = (GameObject)Instantiate(pooledObject);
			obj.SetActive(false);
			ObjectList.Add(obj);
		}
	}

	public GameObject getObject(){
		for (int i = 0; i < ObjectList.Count; i++) {
			if(!ObjectList[i].activeInHierarchy){
				return ObjectList[i];
			}
		}

		GameObject obj = (GameObject)Instantiate(pooledObject);
		obj.SetActive(false);
		ObjectList.Add(obj);
		return obj;
	}
}
