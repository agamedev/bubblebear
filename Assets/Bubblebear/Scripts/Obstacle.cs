﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {


	protected virtual IEnumerator delete (GameObject obs){
		yield return new WaitForSeconds (7);
		Destroy (obs);
	}


	protected IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time){
		var i= 0.0f;
		var rate= 1.0f/time;
		while (i < 1.0f) {
			i += Time.deltaTime * rate;
			thisTransform.position = Vector3.Lerp(startPos, endPos, i);
			yield return null; 
		}
	}
	


}
