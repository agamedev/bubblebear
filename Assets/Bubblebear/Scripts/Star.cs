﻿using UnityEngine;
using System.Collections;

public class Star : MonoBehaviour {
	public GameObject star;
	public GameObject[] powerups;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("triggerStar",2f,1f);
		InvokeRepeating ("triggerPowerUp",30f,10f);
	}

	void triggerStar(){
		float pos = Random.Range (-2.0f, 2.0f);
		Instantiate (star, new Vector2 (pos, transform.position.y - 2f), Quaternion.identity);
		
	}

	void triggerPowerUp(){
		int ran = Random.Range (0,4);
		int pos = Random.Range (-2,2);
		StartCoroutine (spawnPowerUp (ran, pos));
	}

	IEnumerator spawnPowerUp(int ran, int pos){
		int time = Random.Range (15,30);
		yield return new WaitForSeconds (time);
		Instantiate (powerups [ran], new Vector2 (pos, transform.position.y - 2.2f), Quaternion.identity);
		yield return new WaitForSeconds (time + 10);
	}
}
