using UnityEngine;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	public GameObject[] invenSkill = new GameObject[3];
	Powerup pu;
	// Use this for initialization
	void Start () {
		pu = Camera.main.GetComponent<Powerup> ();
		invenSkill [2].name = "";
		invenSkill [1].name = "";
		invenSkill [0].name = "";

	}
	
	// Update is called once per frame
	void Update () {
	
#if UNITY_ANDROID
		if (Input.touchCount > 0){ 
			for (int i = 0; i < Input.touchCount; i++){
				Touch currentTouch = Input.GetTouch(i);
				if (currentTouch.phase == TouchPhase.Began){
					Vector2 v2 = new Vector2(Camera.main.ScreenToWorldPoint(currentTouch.position).x, Camera.main.ScreenToWorldPoint(currentTouch.position).y);
					Collider2D c2d = Physics2D.OverlapPoint(v2);
					
					if (c2d != null){
						switch(c2d.gameObject.name){
						case "Time Slow" : 
							delItem(c2d.gameObject);
							pu.getPowerup("TimeSlow"); 
							break;
						case "Clean Zone" : 
							delItem(c2d.gameObject);
							pu.getPowerup("CleanZone"); 
							break;
						case "Armor" : 
							delItem(c2d.gameObject);
							pu.getPowerup("Armour"); 
							break;
						case "Haste Fly" : 
							delItem(c2d.gameObject);
							pu.getPowerup("HasteFly"); 
							break;
						case "Invisible" : 
							delItem(c2d.gameObject);
							pu.getPowerup("Invincible"); 
							break;
						default: break;
						}
					}
				}
			}
		}

#endif
#if UNITY_EDITOR
		if (Input.GetMouseButtonDown (0)) {
			Vector2 v2 = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
			Collider2D c2d = Physics2D.OverlapPoint (v2);

			if (c2d != null) {
				switch(c2d.gameObject.name){
					case "Time Slow" : 
							delItem(c2d.gameObject);
							pu.getPowerup("TimeSlow"); 
								break;
					case "Clean Zone" : 
							delItem(c2d.gameObject);
							pu.getPowerup("CleanZone"); 
								break;
					case "Armor" : 
							delItem(c2d.gameObject);
							pu.getPowerup("Armour"); 
								break;
					case "Haste Fly" : 
							delItem(c2d.gameObject);
							pu.getPowerup("HasteFly"); 
								break;
					case "Invisible" : 
							delItem(c2d.gameObject);
							pu.getPowerup("Invincible"); 
								break;
					default: break;
				}
			}
		}
#endif

	}

	void delItem(GameObject obj){
		obj.name = "";
		obj.GetComponent<SpriteRenderer> ().sprite = null;
	}

	public void getItem(GameObject obj){
		Items x = obj.GetComponent<Items> ();

			if (invenSkill[0].name != "") {	
				swapItem();
			}

			invenSkill [0].name = x.nama;
			invenSkill [0].GetComponent<SpriteRenderer> ().sprite = x.icon;
	
	}

	void processSwap(int i){
		if (i == 0) {
			invenSkill [i].name = "";
			invenSkill [i].GetComponent<SpriteRenderer> ().sprite = null;

		} else {
			invenSkill [i].name = invenSkill [i - 1].name;
			invenSkill [i].GetComponent<SpriteRenderer> ().sprite = invenSkill [i - 1].GetComponent<SpriteRenderer> ().sprite;
		}

	}
	void swapItem(){
		if (invenSkill [1].name == "" && invenSkill [2].name != "" && invenSkill [0].name != "") {
			processSwap (1);
			processSwap (0);
		} else {
			processSwap (2);
			processSwap (1);
			processSwap (0);
		}
	}
}