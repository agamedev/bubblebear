﻿using UnityEngine;
using System.Collections;

public class GroundDestroy : MonoBehaviour {
	GameObject destroyPoint;
	// Use this for initialization
	void Start () {
		destroyPoint = GameObject.Find ("Destroy");
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y < destroyPoint.transform.position.y) {
			if (gameObject.tag == "bg") {
				Destroy(gameObject);
			} else {
				gameObject.SetActive(false);
			}
		}
	}

}
